# Emulsify Mutant: Pattern Lab + Drupal 8

Component-driven _sandbox_ Drupal theme using [Pattern Lab v2](http://patternlab.io/) automated via Gulp/NPM.    
Based on [Emulsify (v2.2)](https://github.com/fourkitchens/emulsify)

## Requirements

  1. [Node (we recommend NVM)](https://github.com/creationix/nvm)
  2. [Gulp](http://gulpjs.com/)
  3. [Composer](https://getcomposer.org/)
  4. Optional: [Yarn](https://github.com/yarnpkg/yarn)
